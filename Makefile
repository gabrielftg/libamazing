CFLAGS ?= -Wall -Werror -g0 -O0 -save-temps

LOADERPATH = -L.
HEADERPATH = -Isrc/

SONAME = libamazing.so.2
SONAMEGEN = -Wl,-soname=$(SONAME)

VERSIONGEN = -Wl,--version-script src/Versions

.PHONY:
all: $(SONAME)

.PHONY:
$(SONAME): libamazing.so
	ln -s $< $@

libamazing.so: amazing.os
	gcc $(CFLAGS) $^ -shared -o $@ $(SONAMEGEN) $(VERSIONGEN)

%.os: src/%.c
	gcc $(CFLAGS) $^ -c -fpie -o $@ $(HEADERPATH)

.PHONY:
install: all
	install -d usr/include
	install -d usr/lib
	install -t usr/include/ src/amazing.h
	install -t usr/lib/ libamazing.so
	install -t usr/lib/ $(SONAME)

.PHONY:
uninstall:
	rm -rf usr/

.PHONY:
check: test-amazing test-amazing-compat-single test-amazing-compat-double
	LD_LIBRARY_PATH=usr/lib/ ./test-amazing
	LD_LIBRARY_PATH=usr/lib/ ./test-amazing-compat-single
	LD_LIBRARY_PATH=usr/lib/ ./test-amazing-compat-double

test-amazing: tests/test-amazing.c
	gcc $(CFLAGS) $< -lamazing -o $@ $(HEADERPATH) $(LOADERPATH)

test-amazing-compat-single: tests/test-amazing.c
	gcc $(CFLAGS) $< -lamazing -o $@ $(HEADERPATH) $(LOADERPATH) -D__LIBAMAZING_COMPAT_SINGLE

test-amazing-compat-double: tests/test-amazing.c
	gcc $(CFLAGS) $< -lamazing -o $@ $(HEADERPATH) $(LOADERPATH) -D__LIBAMAZING_COMPAT_DOUBLE

.PHONY:
clean:
	rm -f *.o *.os *.so *.s *.i
	rm -f libamazing.so.*
	rm -f test-amazing test-amazing-compat-single test-amazing-compat-double
