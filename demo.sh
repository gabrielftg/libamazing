#!/bin/bash

set -e

# Clean the repository before starting
git stash
git reset HEAD
git checkout .
git clean -xffd

# Build every version of the library, then install it and the test
# program to a location that contains version information (the test
# program will be reused against many different install locations).
for version in 0.1 0.2 0.3 1.0 1.1 1.2
do
  git checkout libamazing-${version}
  make all
  make install
  make check
  rsync -a usr version-${version}/
  rsync -a test-amazing version-${version}/bin/
  make uninstall
  make clean
done

# Return to master branch
git checkout master

set +e

fake_soname () {
  for version in 0.1 0.2
  do
    ln -s libamazing.so version-${version}/usr/lib/libamazing.so.2
  done
  for version in 0.3 1.0 1.1 1.2
  do
    ln -s libamazing.so version-${version}/usr/lib/libamazing.so.1
  done
}

unfake_soname () {
  for version in 0.1 0.2
  do
    unlink version-${version}/usr/lib/libamazing.so.2
  done
  for version in 0.3 1.0 1.1 1.2
  do
    unlink version-${version}/usr/lib/libamazing.so.1
  done
}

# Normal tests
echo
echo Starting tests...
echo
echo Load each test case using the same library it was linked against.
echo
for version in 0.1 0.2 0.3 1.0 1.1 1.2
do
  echo -n "libamazing-${version}: "
  LD_LIBRARY_PATH=version-${version}/usr/lib version-${version}/bin/test-amazing
done

for program in 0.1 0.2 0.3 1.0 1.1 1.2
do
  echo
  echo Test program version ${program}
  echo
  for library in 0.1 0.2 0.3 1.0 1.1 1.2
  do
    echo -n "libamazing-${library}: "
    LD_LIBRARY_PATH=version-${library}/usr/lib version-${program}/bin/test-amazing
  done
  echo
  echo Test program version ${program} - fake SONAME
  echo
  fake_soname
  for library in 0.1 0.2 0.3 1.0 1.1 1.2
  do
    echo -n "libamazing-${library}: "
    LD_LIBRARY_PATH=version-${library}/usr/lib version-${program}/bin/test-amazing
  done
  unfake_soname
done
