#include <stdio.h>

#include <amazing.h>

int
main (void)
{
  double result;

  result = libamazing_constant ();
  fprintf (stdout, "Amazing constant = %f\n", result);

#if defined __LIBAMAZING_COMPAT_SINGLE
  if ((float) AMAZING_CONSTANT != result)
#elif defined __LIBAMAZING_COMPAT_DOUBLE
  if (AMAZING_CONSTANT != result)
#else
  if (12.0 != result)
#endif
    return 1;

  return 0;
}
