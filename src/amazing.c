/* Amazing library with amazingly useless functions.  */

#include <amazing.h>
#include <macros.h>

/* New version of libamazing_constant with new internal behaviour
   (return 12, because it has always been an amazing constant).  */
double
__new_double_libamazing_constant (void)
{
  return 12.0;
}
versioned_symbol (__new_double_libamazing_constant, libamazing_constant, libamazing_1.2);

/* Double-precision floating-point constant.  */
double
__double_libamazing_constant (void)
{
  return DOUBLE_PRECISION (AMAZING_CONSTANT);
}
compat_symbol (__double_libamazing_constant, libamazing_constant, libamazing_1.1);

/* Single-precision floating-point is not amazingly enough, so this
   version of libamazing_constant was superseded by a new version with
   double-precision, above.  */
float
__float_libamazing_constant (void)
{
  return SINGLE_PRECISION (AMAZING_CONSTANT);
}
compat_symbol (__float_libamazing_constant, libamazing_constant, libamazing_0.2);

/* Older versions of libamazing_constant returned an integer typed
   variable with the hardcoded value 12.  */
int
__int_libamazing_constant (void)
{
  return 12;
}
compat_symbol (__int_libamazing_constant, libamazing_constant, libamazing_0.1);
