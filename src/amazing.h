/* Amazing library with amazingly useless functions.  */

#define __LIBAMAZING_E		2.718281828459045235360287471352662498
#define __LIBAMAZING_PI		3.141592653589793238462643383279502884
#define __LIBAMAZING_SQRT2	1.414213562373095048801688724209698079

#define AMAZING_CONSTANT __LIBAMAZING_SQRT2

#ifdef __LIBAMAZING_COMPAT_SINGLE
float
#else
double
#endif
libamazing_constant (void);

#define __LIBAMAZING_REDIRECT(from, to) \
  extern __typeof (from) to; \
  extern __typeof (from) from __asm__ (#to);

#ifdef __LIBAMAZING_COMPAT_SINGLE
__LIBAMAZING_REDIRECT (libamazing_constant, __float_libamazing_constant);
#endif

#ifdef __LIBAMAZING_COMPAT_DOUBLE
__LIBAMAZING_REDIRECT (libamazing_constant, __double_libamazing_constant);
#endif
