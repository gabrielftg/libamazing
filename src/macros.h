/* Amazing library with amazingly useless functions.  */

#define SINGLE_PRECISION_X(x) x##f
#define DOUBLE_PRECISION_X(x) x
#define SINGLE_PRECISION(x) SINGLE_PRECISION_X (x)
#define DOUBLE_PRECISION(x) DOUBLE_PRECISION_X (x)

#define strong_alias(function_name, alias_name) \
  extern typeof (function_name) alias_name __attribute__ ((alias (#function_name)));

#define weak_alias(function_name, alias_name) \
  extern typeof (function_name) alias_name __attribute__ ((weak, alias (#function_name)));

#define versioned_symbol(function_name, symbol_name, symbol_version) \
  asm (".symver " #function_name "," #symbol_name "@@" #symbol_version);

#define compat_symbol(function_name, symbol_name, symbol_version) \
  asm (".symver " #function_name "," #symbol_name "@" #symbol_version);
